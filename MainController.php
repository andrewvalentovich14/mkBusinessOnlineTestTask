<?php

/*
	task
	1. Напишите функцию подготовки строки, которая заполняет шаблон данными из указанного объекта
	2. Пришлите код целиком, чтобы можно его было проверить
	3. Придерживайтесь code style текущего задания
	4. По необходимости - можете дописать код, методы
	5. Разместите код в гите и пришлите ссылку
*/

/**
 * Класс обработчик или контроллер
 *
 * @author		Andrew Valentovich
 * @version		v.1.1 (12/09/2022)
 */
class MainController
{
    public function main()
    {
        $user =
            [
                'id'		=> 20,
                'name'		=> 'John Dow',
                'role'		=> 'QA',
                'salary'	=> 100
            ];


        $api_path_templates =
            [
                "/api/items/%id%/%name%",
                "/api/items/%id%/%role%",
                "/api/items/%id%/%salary%"
            ];


        $api = new Api();


        /**
         * Получаем массив готовых запросов
         *
         * @author		Andrew Valentovich
         * @version		v.1.0 (12/09/2022)
         * @param		array $api_paths
         */
        $api_paths = array_map(function ($api_path_template) use ($api, $user)
        {
            return $api->get_api_path($user, $api_path_template);
        }, $api_path_templates);

        return json_encode($api_paths, JSON_FORCE_OBJECT | JSON_UNESCAPED_UNICODE);

//        $expected_result = ['/api/items/20/John%20Dow','/api/items/20/QA','/api/items/20/100'];
    }
}
