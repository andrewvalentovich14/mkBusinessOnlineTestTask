<?php

/**
 * Класс для работы с API
 *
 * @author		Andrew Valentovich
 * @version		v.1.1 (12/09/2022)
 */
class Api
{
    public function __construct()
    {

    }

    /**
     * Заполняет строковый шаблон template данными из объекта object
     *
     * @author		Andrew Valentovich
     * @version		v.1.0 (12/09/2022)
     * @param		array $array
     * @param		string $template
     * @return		string
     */
    public function get_api_path(array $array, string $template) : string
    {
        $result = $template;

        foreach ($array as $key => $value) {
            $result = preg_replace("/%$key%/", rawurlencode($value), $result);
        }

        return $result;
    }
}
